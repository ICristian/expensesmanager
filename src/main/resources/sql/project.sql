# CREATE TABLE `project` (
#   `projectId` int(11) NOT NULL AUTO_INCREMENT,
#   `projectName` varchar(45) NOT NULL,
#   `client` varchar(45) NOT NULL,
#   `stage` varchar(10) NOT NULL,
#   `userId` int(11) NOT NULL,
#   PRIMARY KEY (`projectId`),
#   KEY `project_user_fk_idx` (`userId`),
#   CONSTRAINT `project_user_fk` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE
# )


CREATE TABLE `project` (
                           `project_id` bigint(20) NOT NULL AUTO_INCREMENT,
                           `client` varchar(255) DEFAULT NULL,
                           `projectname` varchar(255) DEFAULT NULL,
                           `stage` varchar(255) DEFAULT NULL,
                           `user_id` bigint(20) DEFAULT NULL,
                           PRIMARY KEY (`project_id`),
                           KEY `FKo06v2e9kuapcugnyhttqa1vpt` (`user_id`)
)