

# CREATE TABLE `role` (
#                         `roleId` int(11) NOT NULL AUTO_INCREMENT,
#                         `roleName` varchar(10) NOT NULL,
#                         PRIMARY KEY (`roleId`)
# )

CREATE TABLE `role` (
                        `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
                        `rolename` varchar(255) DEFAULT NULL,
                        PRIMARY KEY (`role_id`)
)