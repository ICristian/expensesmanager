# CREATE TABLE `userrole` (
#   `userroleId` int(11) NOT NULL AUTO_INCREMENT,
#   `userId` int(11) NOT NULL,
#   `roleId` int(11) NOT NULL,
#   PRIMARY KEY (`userroleId`),
#   KEY `userrole_user_fk_idx` (`userId`),
#   KEY `userrole_role_fk_idx` (`roleId`),
#   CONSTRAINT `userrole_role_fk` FOREIGN KEY (`roleId`) REFERENCES `role` (`roleId`) ON DELETE CASCADE ON UPDATE CASCADE,
#   CONSTRAINT `userrole_user_fk` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE
# )

CREATE TABLE `user_roles` (
                              `users_user_id` bigint(20) NOT NULL,
                              `roles_role_id` bigint(20) NOT NULL,
                              PRIMARY KEY (`users_user_id`,`roles_role_id`),
                              KEY `FKhxmmg8j4h4qpwbvf39cnujlkf` (`roles_role_id`)
)