# -- sql script for transaction table
# CREATE TABLE `transaction` (
#   `transactionId` int(11) NOT NULL AUTO_INCREMENT,
#   `ammount` int(11) NOT NULL,
#   `description` varchar(45) DEFAULT NULL,
#   `categoryId` int(11) NOT NULL,
#   `projectId` int(11) NOT NULL,
#   PRIMARY KEY (`transactionId`),
#   KEY `transacation_project_fk_idx` (`projectId`),
#   KEY `transaction_category_fk_idx` (`categoryId`),
#   CONSTRAINT `transacation_project_fk` FOREIGN KEY (`projectId`) REFERENCES `project` (`projectId`) ON DELETE CASCADE ON UPDATE CASCADE,
#   CONSTRAINT `transaction_category_fk` FOREIGN KEY (`categoryId`) REFERENCES `category` (`categoryId`) ON DELETE CASCADE ON UPDATE CASCADE
# )

CREATE TABLE `transaction` (
                               `transaction_id` bigint(20) NOT NULL AUTO_INCREMENT,
                               `ammount` int(11) DEFAULT NULL,
                               `description` varchar(255) DEFAULT NULL,
                               `category_id` bigint(20) DEFAULT NULL,
                               `project_id` bigint(20) DEFAULT NULL,
                               PRIMARY KEY (`transaction_id`),
                               KEY `FKgik7ruym8r1n4xngrclc6kiih` (`category_id`),
                               KEY `FKh0tf5dukhwwwiivhdy6jbkp5x` (`project_id`)
)