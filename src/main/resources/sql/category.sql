# CREATE TABLE `category` (
#   `categoryId` int(11) NOT NULL AUTO_INCREMENT,
#   `categoryName` varchar(45) NOT NULL,
#   PRIMARY KEY (`categoryId`)
# )

CREATE TABLE `category` (
                            `category_id` bigint(20) NOT NULL AUTO_INCREMENT,
                            `categoryname` varchar(255) DEFAULT NULL,
                            PRIMARY KEY (`category_id`)
)