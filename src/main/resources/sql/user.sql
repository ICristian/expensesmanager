

# CREATE TABLE `user` (
#                         `userId` int(11) NOT NULL AUTO_INCREMENT,
#                         `userName` varchar(45) NOT NULL,
#                         `password` varchar(45) NOT NULL,
#                         PRIMARY KEY (`userId`)
# )

CREATE TABLE `user` (
                        `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
                        `password` varchar(255) DEFAULT NULL,
                        `username` varchar(255) DEFAULT NULL,
                        PRIMARY KEY (`user_id`)
)