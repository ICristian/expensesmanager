# CREATE TABLE `account` (
#   `accountID` int(11) NOT NULL AUTO_INCREMENT,
#   `categoryId` int(11) NOT NULL,
#   `projectId` int(11) NOT NULL,
#   `ammount` int(11) DEFAULT NULL,
#   PRIMARY KEY (`accountID`),
#   KEY `account_project_fk_idx` (`projectId`),
#   KEY `account_category_fk_idx` (`categoryId`),
#   CONSTRAINT `account_category_fk` FOREIGN KEY (`categoryId`) REFERENCES `category` (`categoryId`) ON DELETE CASCADE ON UPDATE CASCADE,
#   CONSTRAINT `account_project_fk` FOREIGN KEY (`projectId`) REFERENCES `project` (`projectId`) ON DELETE CASCADE ON UPDATE CASCADE
# )

CREATE TABLE `account` (
                           `account_id` bigint(20) NOT NULL AUTO_INCREMENT,
                           `ammount` int(11) DEFAULT NULL,
                           `category_id` bigint(20) DEFAULT NULL,
                           `project_id` bigint(20) DEFAULT NULL,
                           PRIMARY KEY (`account_id`),
                           KEY `FK9wcm1rgyueqec9lwxaqy6lgfo` (`category_id`),
                           KEY `FK14lm6w9jdt2h8la4mrd3dagi0` (`project_id`)
)