package com.project.expensesmanager;

import com.project.expensesmanager.security.service.UserServiceImpl;
import com.project.expensesmanager.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableCaching
public class ExpensesmanagerApplication implements CommandLineRunner {


    @Autowired
    UserServiceImpl userService;
    @Autowired
    RoleServiceImpl roleService;
    @Autowired
    CategoryServiceImpl categoryService;
    @Autowired
    ProjectServiceImpl projectService;
    @Autowired
    TransactionServiceImpl transactionService;
    @Autowired
    AccountServiceImpl accountService;

    public static void main(String[] args) {
        SpringApplication.run(ExpensesmanagerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        User user = new User("Cristian","best");
//        userService.save(user);
//        Role role = new Role("user");
//        roleService.saveRole(role);
//        Role role1= new Role("14");
//        Role role2 = new Role("15");
//        Set<Role> roles= new HashSet<>();
//        roles.add(role1);
//        roles.add(role2);
//        User user2 = new User("fff","pfp");
//        user2.setRoles(roles);
//        userService.save(user2);

//        User userFoundById = userService.findById(4l);
//        Project project1 = new Project("bucatarie", "Popescu", "dev");
//        Project project2 = new Project("living", "Popescu", "dev");
//        project1.setUser(userService.findById(4l));
//        project2.setUser(userService.findById(4l));
//        projectService.save(project1);
//        projectService.save(project2);
//     System.out.println("LISTA :  " + projectService.findAllByUserId(4l).get(0).getProjectName());
//        Transaction transaction = new Transaction(200,"Manere Feromob ");
//        transaction.setCategory(categoryService.findById(2l));
//        transaction.setProject(projectService.findById(2l));
//        transactionService.save(transaction);
//
//        Account account = new Account(categoryService.findById(2l),projectService.findById(2l), 800);
//        accountService.save(account);
    }

    public  UserServiceImpl getUserService() {
        return userService;
    }

    public  void setUserService(UserServiceImpl userService) {
        userService = userService;
    }




}
