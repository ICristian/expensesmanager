package com.project.expensesmanager.model;


import com.project.expensesmanager.security.model.User;
import com.project.expensesmanager.service.AccountService;
import com.project.expensesmanager.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table (name = "project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long projectId;
    @Column(name= "projectname")
    private String projectName;
    @Column(name= "client")
    private String client;
    @Column(name= "stage")
    private String stage;
    @ManyToOne
    @JoinColumn(name="userId")
    private User user;
    @OneToMany(mappedBy= "project")
    private Set<Account>accounts = new HashSet<>();
    @OneToMany(mappedBy= "project")
    private Set<Transaction>transactions = new HashSet<>();











    public Project() {
    }

    public Project(String projectName, String client, String stage, User user) {
        this.projectName = projectName;
        this.client = client;
        this.stage = stage;
        this.user = user;
    }

    public Project(String projectName, String client, String stage) {
        this.projectName = projectName;
        this.client = client;
        this.stage = stage;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(Set<Account> accounts) {
        this.accounts = accounts;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }


}


