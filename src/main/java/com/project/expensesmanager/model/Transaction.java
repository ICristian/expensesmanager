package com.project.expensesmanager.model;

import javax.persistence.*;

@Entity
@Table(name = "transaction")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long transactionId;
    @Column (name= "ammount")
    private int ammount;
    @Column (name = "description")
    private String description;
    @ManyToOne
    @JoinColumn(name= "categoryId")
    private Category category;
    @ManyToOne
    @JoinColumn(name = "projectId")
    private Project project;




    public Transaction() {
    }

    public Transaction(int ammount, String description) {
        this.ammount = ammount;
        this.description = description;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public int getAmmount() {
        return ammount;
    }

    public void setAmmount(int ammount) {
        this.ammount = ammount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
