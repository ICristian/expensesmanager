package com.project.expensesmanager.model;


import javax.persistence.*;

@Entity
@Table (name = "account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long accountId;
   @ManyToOne
   @JoinColumn(name= "categoryId")
    private Category category;
   @ManyToOne
   @JoinColumn(name = "projectId")
    private Project project;
   @Column (name = "ammount")
    private int ammount;

   @Transient
   private int sold;

    public Account() {
    }

    public Account(Category category, Project project, int ammount) {
        this.category = category;
        this.project = project;
        this.ammount = ammount;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public int getAmmount() {
        return ammount;
    }

    public void setAmmount(int ammount) {
        this.ammount = ammount;
    }

    public int getSold() {
        return sold;
    }

    public void setSold(int sold) {
        this.sold = sold;
    }
}
