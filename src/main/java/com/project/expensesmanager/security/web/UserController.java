package com.project.expensesmanager.security.web;

import com.project.expensesmanager.model.Project;
import com.project.expensesmanager.security.model.User;
import com.project.expensesmanager.security.service.SecurityService;
import com.project.expensesmanager.security.service.UserService;
import com.project.expensesmanager.security.validator.UserValidator;
import com.project.expensesmanager.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
    public class UserController {
        @Autowired
        private UserService userService;

        @Autowired
        private SecurityService securityService;

        @Autowired
        private UserValidator userValidator;

        @Autowired
        private ProjectService projectService;

        @GetMapping("/registration")
        public String registration(Model model) {
            model.addAttribute("userForm", new User());

            return "registration";
        }

        @PostMapping("/registration")
        public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult) {
            userValidator.validate(userForm, bindingResult);

            if (bindingResult.hasErrors()) {
                return "registration";
            }

            userService.save(userForm);

            securityService.autoLogin(userForm.getUserName(), userForm.getPasswordConfirm());

            return "redirect:/welcome";
        }

        @GetMapping("/login")
        public String login(Model model, String error, String logout) {
            if (error != null)
                model.addAttribute("error", "Your username and password is invalid.");

            if (logout != null)
                model.addAttribute("message", "You have been logged out successfully.");

            return "login";
        }

        @GetMapping({"/", "/welcome"})
        public String welcome(Model model) {
            Long currentUserId = userService.findByUsername(securityService.findLoggedInUsername()).getUserId();
            List<Project> projects = projectService.findAllByUserId(currentUserId);
//         Adding the list of projects to the model in order to display it
            model.addAttribute("projectUserList", projects);
            // this is the name of the jsp file that will be used to display the page
            return "projectsByUserId";
        }




    }
