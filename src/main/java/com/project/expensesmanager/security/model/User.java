package com.project.expensesmanager.security.model;
import com.project.expensesmanager.model.Project;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table (name= "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;
    @Column(name= "username")
    private String userName;
    @Column(name="password")
    private String password;

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name="user_roles",
                joinColumns = @JoinColumn(name = "users_user_id"),
                inverseJoinColumns = @JoinColumn(name="roles_role_id"))

    private Set<Role> roles = new HashSet<>();

    @Transient
    private String passwordConfirm;

    @OneToMany(mappedBy ="user")
    private Set<Project>projects = new HashSet<>();

    public User(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public User() {
    }


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
