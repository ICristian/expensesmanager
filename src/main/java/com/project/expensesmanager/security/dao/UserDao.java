package com.project.expensesmanager.security.dao;


import com.project.expensesmanager.security.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserDao extends JpaRepository<User, Long> {


}
