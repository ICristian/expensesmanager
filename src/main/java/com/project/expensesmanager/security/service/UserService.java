package com.project.expensesmanager.security.service;

import com.project.expensesmanager.security.model.User;
import java.util.List;


public interface UserService {

    void save(User user);
    User findById(Long id);
    User findByUsername(String userName);
    List<User> findAll ();
    void deleteById(Long id);
    void update(Long id, String newUserName, String newPassword);

}
