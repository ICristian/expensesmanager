package com.project.expensesmanager.security.service;

import com.project.expensesmanager.security.dao.RoleDao;
import com.project.expensesmanager.security.dao.UserDao;
import com.project.expensesmanager.security.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(new HashSet<>(roleDao.findAll()));

        userDao.save(user);

    }

    @Override
    public User findById(Long id) {
        User user = userDao.findById(id).get();
        return user;
    }

    @Override
    public User findByUsername(String userName) {
        ArrayList<User>allUsers = new ArrayList<>(findAll());
        User user = new User();
        for(User currentUser:allUsers){
            if(currentUser.getUserName().equals(userName)){
                 user= currentUser;
            }
        }
        return user;
    }

    @Override
    public List<User> findAll() {
        List<User> allUsers = userDao.findAll();
        return allUsers;
    }

    @Override
    public void deleteById(Long id) {
        userDao.deleteById(id);

    }

    @Override
    public void update(Long id, String newUserName, String newPassword) {
        User user = findById(id);
        user.setUserName(newUserName);
        user.setPassword(newPassword);
        userDao.save(user);
    }
}
