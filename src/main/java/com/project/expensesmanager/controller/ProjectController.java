package com.project.expensesmanager.controller;

import java.util.ArrayList;
import java.util.List;

import com.project.expensesmanager.model.Account;
import com.project.expensesmanager.model.Project;
import com.project.expensesmanager.model.Transaction;
import com.project.expensesmanager.security.model.User;
import com.project.expensesmanager.service.ProjectService;
import com.project.expensesmanager.security.service.SecurityService;
import com.project.expensesmanager.security.service.UserService;
import com.project.expensesmanager.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;



@Controller
@RequestMapping("/project")
public class ProjectController {

    @Autowired
    private ProjectService projectService;
    @Autowired
    private UserService userService;
    @Autowired
    private SecurityService securityService;
    @Autowired
    private TransactionService transactionService;





    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String delete(ModelMap model, @RequestParam("projectId") Long projectId) {

         Long currentUserId = userService.findByUsername(securityService.findLoggedInUsername()).getUserId();
        // deleting the project from database
        projectService.deleteById(projectId);
//        // this is the name of the attribute that will be iterated in the jsp file
        // this is the name of the jsp file that will be used to display the page
        List<Project> projects = projectService.findAllByUserId(currentUserId);
        model.addAttribute("projectUserList" , projects);

        return "projectsByUserId";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createProject(ModelMap model, @RequestParam("projectName") String newProjectName,
                                                @RequestParam("client") String newClient,
                                                @RequestParam("stage") String newStage) {
        Long currentUserId = userService.findByUsername(securityService.findLoggedInUsername()).getUserId();
        User currentUser = userService.findById(currentUserId);
        projectService.saveWithParameters(newProjectName, newClient, newStage, currentUser);

        model.addAttribute("projectList", projectService.findAll());
//        refresh  projectsByUserId List
        List<Project> projects = projectService.findAllByUserId(currentUserId);
        model.addAttribute("projectUserList" , projects);


        return "projectsByUserId";
    }

    @RequestMapping(value = "/details", method = RequestMethod.GET)
    public String projectView(ModelMap model, @RequestParam("projectId") Long projectViewId) {
        Project projectToView = projectService.findById(projectViewId);
        List<Account>accountsOfThisProject = new ArrayList<>(projectToView.getAccounts());
        List<Transaction>transactionsOfThisProject = new ArrayList<>(transactionService.findAllTransactionsByProjectId(projectViewId));
        int sumOfAmmountsOfAccounts=0;
        for(Account currentAccount: accountsOfThisProject){
            sumOfAmmountsOfAccounts= sumOfAmmountsOfAccounts + currentAccount.getAmmount();
        }

        for(Account a:accountsOfThisProject){
            int sum=0;
           for(Transaction t:transactionsOfThisProject){
               if(t.getCategory().getCategoryName().equals(a.getCategory().getCategoryName())){
                   sum=sum+t.getAmmount();
               }
           }
           a.setSold(a.getAmmount()-sum);
        }

        List<String>categories = projectService.getCategoriesOfThisProject(projectViewId);

        model.addAttribute("categories",categories);
        model.addAttribute("sumOfAccounts",sumOfAmmountsOfAccounts);
        model.addAttribute("userproject", projectToView);
        model.addAttribute("projectAccounts",accountsOfThisProject);
        model.addAttribute("projectTransactions", transactionsOfThisProject);
        return "projectDetails";
    }




    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public String updateProjectView(ModelMap model, @RequestParam("projectId") Long projectToBeUpdatedId) {
        Project projectToBeUpdated = projectService.findById(projectToBeUpdatedId);
        model.addAttribute("project", projectToBeUpdated);
        return "projectUpdate";
    }
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateProjectSave(ModelMap model, @RequestParam("projectId") Long projectId,
                                                    @RequestParam("projectName") String projectName,
                                                    @RequestParam("client") String client,
                                                    @RequestParam("stage") String stage,
                                                    @RequestParam("userName" ) String userName){
        User user = userService.findByUsername(userName);
        projectService.update(projectId,projectName,client,stage, user);
//        refresh the list of projects, then send them to  jsp
        Long currentUserId = userService.findByUsername(securityService.findLoggedInUsername()).getUserId();
        List<Project> projects = projectService.findAllByUserId(currentUserId);
        model.addAttribute("projectUserList" , projects);
        return "projectsByUserId";
    }


}
