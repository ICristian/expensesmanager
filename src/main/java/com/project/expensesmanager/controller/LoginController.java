package com.project.expensesmanager.controller;

import com.project.expensesmanager.model.Project;
import com.project.expensesmanager.security.model.User;
import com.project.expensesmanager.service.ProjectService;
import com.project.expensesmanager.security.service.SecurityService;
import com.project.expensesmanager.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@Controller
    public class LoginController {

        @Autowired
        private UserService userService;
        @Autowired
        private ProjectService projectService;


    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String delete(ModelMap model, @RequestParam("projectId") Long projectId) {

        // deleting the project from database
        projectService.deleteById(projectId);

        // this is the name of the attribute that will be iterated in the jsp file
        model.addAttribute("projectList", projectService.findAll());

        // this is the name of the jsp file that will be used to display the page
        return "projects";
    }



    @RequestMapping(value = "/details", method = RequestMethod.GET)
    public String projectView(ModelMap model, @RequestParam("projectId") Long projectViewId) {
        Project projectToView= projectService.findById(projectViewId);
        model.addAttribute("userproject", projectToView);
        return "details";
    }




    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public String updateProjectView(ModelMap model, @RequestParam("projectId") Long projectToBeUpdatedId) {
        Project projectToBeUpdated = projectService.findById(projectToBeUpdatedId);
        model.addAttribute("project", projectToBeUpdated);
        return "projectUpdate";
    }
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateProjectSave(ModelMap model, @RequestParam("projectId") Long projectId,
                                    @RequestParam("projectName") String projectName,
                                    @RequestParam("client") String client,
                                    @RequestParam("stage") String stage,
                                    @RequestParam("userName" ) String userName){
        User user = userService.findByUsername(userName);

        projectService.update(projectId,projectName,client,stage, user);

        return "projects";
    }







    }

