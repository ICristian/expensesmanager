package com.project.expensesmanager.service;

import com.project.expensesmanager.security.dao.RoleDao;
import com.project.expensesmanager.security.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service

public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleDao roleDao;
    @Override
    public void saveRole(Role role) {
        roleDao.save(role);
    }
}
