package com.project.expensesmanager.service;

import com.project.expensesmanager.dao.AccountDao;
import com.project.expensesmanager.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountServiceImpl  implements AccountService{

    @Autowired
    private AccountDao accountDao;

    @Override
    public void save(Account account) {
        accountDao.save(account);
    }

    @Override
    public Account findById(Long id) {
        Account account = accountDao.getOne(id);
        return account;
    }

    @Override
    public List<Account> findAll() {
        ArrayList<Account> accounts = new ArrayList<>(accountDao.findAll());
        return null;
    }

    @Override
    public void deleteById(Long id) {
        accountDao.deleteById(id);

    }

    @Override
    public List<Account> findAllAccountsByProjectId(Long id) {
        ArrayList<Account> accountsByProjectId= new ArrayList<>();
        ArrayList<Account> accountsAll = new ArrayList<>(findAll());
        for(Account currentAccount:accountsAll){
            if(currentAccount.getProject().getProjectId()==id){
                accountsByProjectId.add(currentAccount);
            }
        }
        return accountsByProjectId;
    }

    @Override
    public Account findAccountByCategoryName(List<Account> accounts, String categoryName) {
        Account selectedAccount = new Account() ;
        for(Account currentAccount:accounts){
            if (currentAccount.getCategory().getCategoryName().equals(categoryName)){
                selectedAccount = currentAccount;
            }
        }
        return selectedAccount;
    }
}
