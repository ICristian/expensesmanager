package com.project.expensesmanager.service;

import com.project.expensesmanager.model.Category;

import java.util.List;

public interface CategoryService {
    void save (Category category);
    Category findById(Long id);
    List<Category> findAll();
    void deleteById (Long id);
}
