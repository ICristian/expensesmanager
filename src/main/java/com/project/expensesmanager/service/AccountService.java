package com.project.expensesmanager.service;

import com.project.expensesmanager.model.Account;

import java.util.List;

public interface AccountService {
    void save (Account account);
    Account findById(Long id);
    List<Account> findAll();
    List<Account> findAllAccountsByProjectId(Long id);
    void deleteById(Long id);
    Account findAccountByCategoryName(List<Account> accounts, String categoryName);
}
