package com.project.expensesmanager.service;

import com.project.expensesmanager.dao.CategoryDao;
import com.project.expensesmanager.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryDao categoryDao;

    @Override
    public void save(Category category) {
        categoryDao.save(category);
    }

    @Override
    public Category findById(Long id) {
        Category category = categoryDao.getOne(id);
        return category;
    }

    @Override
    public List<Category> findAll() {
        ArrayList<Category> categories = new ArrayList<>(categoryDao.findAll());
        return categories;
    }

    @Override
    public void deleteById(Long id) {
        categoryDao.deleteById(id);

    }
}
