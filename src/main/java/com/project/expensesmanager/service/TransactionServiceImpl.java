package com.project.expensesmanager.service;

import com.project.expensesmanager.dao.TransactionDao;
import com.project.expensesmanager.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService{
    @Autowired
    private TransactionDao transactionDao;

    @Override
    public void save(Transaction transaction) {
        transactionDao.save(transaction);
    }

    @Override
    public Transaction findById(Long id) {
        Transaction transaction = transactionDao.getOne(id);
        return transaction;
    }

    @Override
    public List<Transaction> findAll() {
        ArrayList<Transaction> transactions = new ArrayList<>(transactionDao.findAll());
        return transactions;
    }

    @Override
    public List<Transaction> findAllTransactionsByProjectId(Long id) {
        ArrayList<Transaction> transactionsByProjectId= new ArrayList<>();
        ArrayList<Transaction> transactionsAll = new ArrayList<>(findAll());
        for(Transaction currentTransaction:transactionsAll){
            if(currentTransaction.getProject().getProjectId()==id){
                transactionsByProjectId.add(currentTransaction);
            }
        }
        return transactionsByProjectId;
    }

    @Override
    public List<Transaction> findAllTransactionsByCategoryName(List<Transaction> transactions, String categoryName) {
        List<Transaction> transactionsByCategoryName = new ArrayList<>();
        for(Transaction currentTransaction:transactions){
            if(currentTransaction.getCategory().getCategoryName().equals(categoryName)){
                transactionsByCategoryName.add(currentTransaction);
            }
        }
        return transactionsByCategoryName;
    }

    @Override
    public int sumOfAmmounts(List<Transaction> transactions) {
        int sumOfAmmounts = 0;
        for(Transaction currentTransaction: transactions){
            sumOfAmmounts = sumOfAmmounts + currentTransaction.getAmmount();
        }
        return sumOfAmmounts;
    }

    @Override
    public void deleteById(Long id) {
        transactionDao.deleteById(id);
    }


}
