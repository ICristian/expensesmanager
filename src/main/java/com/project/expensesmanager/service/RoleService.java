package com.project.expensesmanager.service;

import com.project.expensesmanager.security.model.Role;

public interface RoleService {
    void saveRole(Role role);
}
