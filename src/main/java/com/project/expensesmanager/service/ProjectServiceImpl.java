package com.project.expensesmanager.service;

import com.project.expensesmanager.dao.ProjectDao;
import com.project.expensesmanager.model.Account;
import com.project.expensesmanager.model.Project;
import com.project.expensesmanager.model.Transaction;
import com.project.expensesmanager.security.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectDao projectDao;
    @Autowired
    TransactionService transactionService;
    @Autowired
    AccountService accountService;

    @Override
    public void save(Project project) {
        projectDao.save(project);
    }

    @Override
    public void saveWithParameters(String name, String client, String stage, User user) {
      Project project = new Project(name, client,stage,user);
      save(project);

    }

    @Override
    public Project findById(Long id) {
        Project project = projectDao.getOne(id);
        return project;
    }

    @Override
    public List<Project> findAll() {
        ArrayList<Project> projects= new ArrayList<>(projectDao.findAll());
        return projects;
    }

    @Override
    public void deleteById(Long id) {
        projectDao.deleteById(id);
    }

    @Override
    public void update(Long id, String newName, String newClient, String newStage, User newUser) {
        Project projectToBeUpdated = findById(id);
        if(newName!=null) {
            projectToBeUpdated.setProjectName(newName);
        }
        if(newClient!=null ){
            projectToBeUpdated.setClient(newClient);
        }
        if(newStage!=null){
            projectToBeUpdated.setStage(newStage);
        }
        if(newUser!=null){
            projectToBeUpdated.setUser(newUser);
        }
        projectDao.save(projectToBeUpdated);

    }
    @Override
    public List<Project> findAllByUserId(Long id){
        ArrayList<Project> projectsById= new ArrayList<>();
        ArrayList<Project> projectsAll = new ArrayList<>(findAll());
        for(Project currentProject:projectsAll){
            if(currentProject.getUser().getUserId()==id){
                projectsById.add(currentProject);
            }
        }
        return projectsById;
    }

    @Override
    public List<String> getCategoriesOfThisProject(Long id){
        List<String> categories = new ArrayList<>();
        for(Account currentAccount:findById(id).getAccounts()){
            categories.add(currentAccount.getCategory().getCategoryName());
        }
        return categories;
    }

    @Override
    public Account getAccountByCategoryName(String categoryName, Project project) {
        Account account= new Account();
        for(Account currentAccount:project.getAccounts() ){
            if(currentAccount.getCategory().getCategoryName().equals(categoryName)){
                account = currentAccount;
            }
        }
        return account;
    }

    @Override
    public int sumOfTransactionsByCategoryName(String category, Project project) {
        int sumOfTransactions = 0;
        for(Transaction currentTransaction: project.getTransactions()){
            if(currentTransaction.getCategory().getCategoryName().equals(category)){
                sumOfTransactions= sumOfTransactions+ currentTransaction.getAmmount();
            }
        }
        return sumOfTransactions;
    }
}


