package com.project.expensesmanager.service;

import com.project.expensesmanager.model.Transaction;

import java.util.List;

public interface TransactionService {
    void save (Transaction transaction);
    Transaction findById(Long id);
    List<Transaction> findAll();
    List<Transaction> findAllTransactionsByProjectId(Long id);
    List<Transaction> findAllTransactionsByCategoryName(List<Transaction> transactions, String categoryName);
    int sumOfAmmounts(List<Transaction> transactions);
    void deleteById(Long id);
}
