package com.project.expensesmanager.service;

import com.project.expensesmanager.model.Account;
import com.project.expensesmanager.model.Project;
import com.project.expensesmanager.security.model.User;

import java.util.List;

public interface ProjectService {
    void save(Project project);

    void saveWithParameters(String name, String client, String stage, User user);

    Project findById(Long id);

    List<Project> findAllByUserId(Long id);

    List<Project> findAll();

    void deleteById(Long id);

    void update(Long id, String projectName, String client, String stage, User user);

    List<String> getCategoriesOfThisProject(Long id);

    Account getAccountByCategoryName(String category, Project project);
    int sumOfTransactionsByCategoryName(String category, Project project);
}