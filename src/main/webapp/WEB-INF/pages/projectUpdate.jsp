<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<body>

    
    <h2>Update existing Project</h2>
    
    <form method="post"
        action="${pageContext.request.contextPath}/project/update">
         New Project Name: <input type="text" name="projectName" value="${project.getProjectName()}"> <br />
         New Client: <input type="text" name="client" value="${project.getClient()}"> <br />
         New Stage <input type="text" name="stage" value="${project.getStage()}"> <br />
         New User: <input type="text" name="userName" value="${project.getUser().getUserName()}"> <br />
        <input type="hidden" name="projectId" value="${project.getProjectId()}">
        <input type="submit" value="Update">
    </form>

</body>
</html>
