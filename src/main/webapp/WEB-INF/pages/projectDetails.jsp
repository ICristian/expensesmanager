<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <title>User Project Details</title>
    <link href="${contextPath}/resources/css/projectDetails.css" rel="stylesheet" >
</head>

<h2>${userproject.getUser().getUserName()}'s Project</h2>
<h2>${userproject.getProjectName()}  ${userproject.getClient()}</h2>

    <body>
            <form method="get"
                  action="${pageContext.request.contextPath}/">
                <p name ="returnTo"></p>
                <input type="submit" name="" value="To Projects">
            </form>

            <form  method="get"
                   action="${pageContext.request.contextPath}/userproject/newtransaction">
                <p name = "newTransaction"></p>
                <input type="submit" name="transaction" value="New Transaction">
            </form>

        <div class="grid-container">
            <div class="item1">
                <table border = 1 >
                    <tr>
                        <th>Project:</th>
                        <td>${userproject.getProjectName()}</td>
                    </tr>
                    <tr>
                        <th>Client:</th>
                        <td>${userproject.getClient()}</td>
                    </tr>
                </table>
                <p allign = "left">PROJECT ACCOUNTS</p>
                <table border = 1 style="float: left">
                    <tr>
                        <th>Category</th>
                        <th>Amount</th>
                        <th>Sold</th>
                    </tr>
                    <c:forEach items="${projectAccounts}" var="account">
                        <tr>
                            <td>${ account.getCategory().getCategoryName()}</td>
                            <td>${account.getAmmount() } lei</td>
                            <td>${account.getSold()}</td>

                        </tr>
                    </c:forEach>
                    <tr>
                        <td>Total </td>
                        <td>${ sumOfAccounts} lei</td>
                        <td>${account.getAmmount()}</td>
                    </tr>
                </table>

                <p>TRANSACTIONS LIST</p>
                <table border = 1 style="margin-left:500px;">
                    <tr>
                        <th>Category</th>
                        <th>Ammount</th>
                        <th>Description</th>

                    </tr>
                    <c:forEach items="${projectTransactions}" var="transaction">
                        <tr>
                            <td>${transaction.getCategory().getCategoryName()} </td>
                            <td>${transaction.getAmmount()} lei</td>
                            <td>${transaction.getDescription()}</td>

                        </tr>
                    </c:forEach>
                </table>
            </div>

<%--            <div class="item2">--%>
<%--                <table border = 1 margin-left:10x>--%>
<%--                    <tr>--%>
<%--                        <th>Client Sold</th>--%>
<%--                        <td>${sold}</td>--%>
<%--                    </tr>--%>
<%--                </table>--%>

<%--                <table border = 1>--%>
<%--                    <tr>--%>
<%--                        <th>Ammount</th>--%>
<%--                        <th>Description</th>--%>
<%--                    </tr>--%>
<%--                    <c:forEach items="${trasactionsProj}" var="transaction">--%>
<%--                        <tr>--%>
<%--                            <td>${transaction.getAmmount }</td>--%>
<%--                            <td>${transaction.getDescription() }</td>--%>
<%--                        </tr>--%>
<%--                    </c:forEach>--%>
<%--                </table>--%>
<%--            </div>--%>
        </div>
    </body>
</html>