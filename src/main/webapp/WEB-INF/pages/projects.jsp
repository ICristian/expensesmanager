<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<body>
<h2>Add new Project</h2>

<form method="post"
	  action="${pageContext.request.contextPath}/project/create">
	Name: <input type="text" name="projectName" > <br />
	Client: <input type="text" name="client" > <br />
	Stage: <input type="text" name="stage" > <br />
	<input type="submit" value="Save">
</form>

<h2>Projects</h2>
	<table border="1">
		<tr>
	       <td>ID</td>
		   <td>User</td>
	       <td>Name</td>
	       <td>Client</td>
		   <td>Stage</td>
	   </tr>
		<c:forEach items="${projectByUserList}" var="project">
			<tr>
				<td>${project.getProjectId() }</td>
				<td>${project.getUser().getUserName() }</td>
				<td>${project.getProjectName() }</td>
				<td>${project.getClient() }</td>
				<td>${project.getStage() }</td>
				<td>
					<form method="get"
						  action="${pageContext.request.contextPath}/project/details">
						<input type="hidden" name="projectId"
							   value="${project.getProjectId()}">
						<input type="submit" value="Details">
					</form>
				</td>
				<td>
					<form method="get"
						  action="${pageContext.request.contextPath}/project/update">
						<input type="hidden" name="projectId"
							   value="${project.getProjectId()}">
						<input type="submit" value="Update">
					</form>
				</td>
				<td>
					<form method="post"
						action="${pageContext.request.contextPath}/project/delete">
						<input type="hidden" name="projectId"
							value="${project.getProjectId()}">
						<input type="submit" value="Delete">
					</form>
				</td>


			</tr>
		</c:forEach>

	</table>
	

	

</body>
</html>
